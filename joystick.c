#include <stdio.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <stdlib.h>
#include <string.h>
#include <usb.h>

#include "joystick.h"

/*

Joystick missile controller.

(c) Dale Maggee, 2010.

Hacked together from ctlmissile.c from (somewhere)
and joystick code at http://scaryreasoner.wordpress.com/2008/02/22/programming-joysticks-with-linux/

GNU GPL.

*/

static int joystick_fd = -1;
static int debug = 1;

int open_joystick()
{
	joystick_fd = open(JOYSTICK_DEVNAME, O_RDONLY | O_NONBLOCK); /* read write for force feedback? */
	if (joystick_fd < 0)
		return joystick_fd;

	/* maybe ioctls to interrogate features here? */

	return joystick_fd;
}

int read_joystick_event(struct js_event *jse)
{
	int bytes;

	bytes = read(joystick_fd, jse, sizeof(*jse)); 

	if (bytes == -1)
		return 0;

	if (bytes == sizeof(*jse))
		return 1;

	printf("Unexpected bytes from joystick:%d\n", bytes);

	return -1;
}

void close_joystick()
{
	close(joystick_fd);
}

int get_joystick_status(struct wwvi_js_event *wjse)
{
	int rc;
	struct js_event jse;
	if (joystick_fd < 0)
		return -1;

	wjse->empty = 1;

	// Allocate 24 bytes (more than we'll ever need) for each joystick
	// movement string, and set them both to the empty string.
	char *s1 = malloc(24), *s2 = malloc(24);
	s1[0] = s2[0] = 0;

	while ((rc = read_joystick_event(&jse) == 1)) {
		jse.type &= ~JS_EVENT_INIT; /* ignore synthetic events */
		if (jse.type == JS_EVENT_AXIS) {
			switch (jse.number) {
			case 0: wjse->stick1_x = jse.value;
				wjse->empty = 0;
				break;
			case 1: wjse->stick1_y = jse.value;
				wjse->empty = 0;
				break;
			case 2: wjse->stick2_x = jse.value;
				wjse->empty = 0;
				break;
			case 3: wjse->stick2_y = jse.value;
				wjse->empty = 0;
				break;
			default:
				break;
			}
		} else if (jse.type == JS_EVENT_BUTTON) {
			if (jse.number < 11) {
				switch (jse.value) {
				case 0:
				case 1: wjse->button[jse.number] = jse.value;
					wjse->empty = 0;
					break;
				default:
					break;
				}
			}
		}
	}

	if (wjse->stick1_x < 0) {
		// Note that what we have below here is a strcpy
		// from statically allocated (compiled-in) memory
		// to heap (malloc)-allocated memory.
		strcpy(s1, "left");
	} else if (wjse->stick1_x > 0) {
		strcpy(s1,"right");
	}

	if (wjse->stick1_y < 0) {
		strcat(s1,"up");
	} else if (wjse->stick1_y > 0) {
		strcat(s1,"down");
	}

	//My gamepad's 2nd stick has an odd orientation...
	if (wjse->stick2_y < 0) {
		strcpy(s2,"left");
	} else if (wjse->stick2_y > 0) {
		strcpy(s2,"right");
	}
	
	if (wjse->stick2_x < 0) {
		strcat(s2,"up");
	} else if (wjse->stick2_x > 0) {
		strcat(s2,"down");
	}

	// pointers to s1 and s2 are now placed in wjse and it's
	// up to the caller to free those strings before they
	// discard wjse.
	wjse->stick1 = s1;
	wjse->stick2 = s2;

	return 0;
}

int detectlauncher(struct usb_device *dev) {
        struct usb_bus *busses, *bus;

        usb_init();
        usb_find_busses();
        usb_find_devices();

        busses = usb_get_busses();

        for (bus = busses; bus && !dev; bus = bus->next) {
                for (dev = bus->devices; dev; dev = dev->next) {
                        if (debug) {
                                printf("Checking 0x%04x:0x%04x\n",
                                        dev->descriptor.idVendor,
                                        dev->descriptor.idProduct);
                        }
                        if (dev->descriptor.idVendor == 0x1130 &&
                                dev->descriptor.idProduct == 0x0202) {
                                if (debug) printf("Found Launcher\n");
                                return 1;
                                break;
                        }
               }
        }

	return 0;

}

void send_command_ms(struct usb_device *dev, char *cmd)
{
        usb_dev_handle *launcher;
        char data[64];
        int ret;

	launcher = usb_open(dev);
	if (launcher == NULL) {
		perror("Unable to open device");
		exit(EXIT_FAILURE);
	}

	/* Detach kernel driver (usbhid) from device interface and claim */
	usb_detach_kernel_driver_np(launcher, 0);
	usb_detach_kernel_driver_np(launcher, 1);

	ret = usb_set_configuration(launcher, 1);
	if (ret < 0) {
		perror("Unable to set device configuration");
		exit(EXIT_FAILURE);
	}

	ret = usb_claim_interface(launcher, 1);
	if (ret < 0) {
		perror("Unable to claim interface");
		exit(EXIT_FAILURE);
	}

	ret = usb_set_altinterface(launcher, 0);
	if (ret < 0) {
		perror("Unable to set alternate interface");
		exit(EXIT_FAILURE);
	}

	data[0] = 'U';
	data[1] = 'S';
	data[2] = 'B';
	data[3] = 'C';
	data[4] = 0;
	data[5] = 0;
	data[6] = 4;
	data[7] = 0;
	ret = usb_control_msg(launcher,
			USB_DT_HID,			// request type
			USB_REQ_SET_CONFIGURATION,	// request
			USB_RECIP_ENDPOINT,		// value
			1,		// index
			data,		// data
			8,		// Length of data.
			500);		// Timeout
	if (ret != 8) {
		fprintf(stderr, "Error: %s\n", usb_strerror());
		exit(EXIT_FAILURE);
	}

	data[0] = 'U';
	data[1] = 'S';
	data[2] = 'B';
	data[3] = 'C';
	data[4] = 0;
	data[5] = 0x40;
	data[6] = 2;
	data[7] = 0;
	ret = usb_control_msg(launcher,
			USB_DT_HID,
			USB_REQ_SET_CONFIGURATION,
			USB_RECIP_ENDPOINT,
			1,
			data,
			8,		// Length of data.
			500);		// Timeout
	if (ret != 8) {
		fprintf(stderr, "Error: %s\n", usb_strerror());
		exit(EXIT_FAILURE);
	}

	usb_set_altinterface(launcher, 0);

	memset(data, 0, 64);
	if (!strcmp(cmd, "up")) {
		data[3] = 1;
	} else if (!strcmp(cmd, "down")) {
		data[4] = 1;
	} else if (!strcmp(cmd, "left")) {
		data[1] = 1;
	} else if (!strcmp(cmd, "right")) {
		data[2] = 1;
	} else if (!strcmp(cmd, "fire")) {
		data[5] = 1;
	} else if (!strcmp(cmd, "rightdown")) {
		data[4] = 1;
		data[2] = 1;
	} else if (!strcmp(cmd, "leftdown")) {
		data[4] = 1;
		data[1] = 1;
	} else if (!strcmp(cmd, "rightup")) {
		data[3] = 1;
		data[2] = 1;
	} else if (!strcmp(cmd, "leftup")) {
		data[3] = 1;
		data[1] = 1;
	} else if (strcmp(cmd, "stop")) {
		fprintf(stderr, "Unknown command: %s", cmd);
		exit(EXIT_FAILURE);
	}

	data[6] = 8;
	data[7] = 8;

	ret = usb_control_msg(launcher,
			USB_DT_HID,
			USB_REQ_SET_CONFIGURATION,
			USB_RECIP_ENDPOINT,
			1,
			data,
			64,		// Length of data.
			1000);		// Timeout
	if (ret != 64) {
		fprintf(stderr, "Error: %s\n", usb_strerror());
		exit(EXIT_FAILURE);
	}

	usb_release_interface(launcher, 1);

	usb_close(launcher);
}


int main(int argc, char *argv[])
{
	int fd, rc;
	int done = 0;
	int b = 0;

	struct js_event jse;
	struct wwvi_js_event evt;

        struct usb_device *dev = NULL;
	if (!detectlauncher(dev)) {
		printf("Cannot find launcher!\n");
	};

	fd = open_joystick();
	if (fd < 0) {
		printf("Cannot open joystick.\n");
		exit(1);
	}

	while (!done) {

        // Let it be known that get_joystick_status allocates two
        // strings in evt.stick1 and evt.stick2, and that it's our
        // (caller's) responsibility to deallocate those.

	if (get_joystick_status(&evt) != 0) {
            // It's more important to check if this function returned
            // successfully or not, now, as a non-successful return
            // won't have allocated stick1/stick2, and if we try to
            // free non-allocated memory, bad things happen.
            fprintf(stderr, "failed to get joystick status.\n");
            return 1;
        }
            

	if (!evt.empty) {
/*			printf("Stick 1:%8hd / %8hd, Stick 2:%8hd / %8hd. Buttons: ",
				evt.stick1_x,evt.stick1_y, evt.stick2_x, evt.stick2_y);

			for (b = 0; b < (sizeof(evt.button) / sizeof(int)); b++) {
				if (evt.button[b]) printf("%2d ",b);
			} 
*/
//			printf(" Stick 1: %s , Stick 2: %s",evt.stick1,evt.stick2);
//			printf("\n");

			//control launcher based on joystick...
		if (strlen(evt.stick1) > 0) {
			//movement...
			printf("Moving %s\n",evt.stick1);
			if (dev) {
				send_command_ms(dev,evt.stick1);
			}
		} else {
			//fire if any button is pressed...
			for (b = 0; b < (sizeof(evt.button) / sizeof(int)); b++) {
				if (evt.button[b]) {
					if (dev) send_command_ms(dev,"fire");
						printf("FIRE!\n");
						break;
					}
				}
				//send stop command when joystick is at home position.
				if (dev) send_command_ms(dev,"stop"); 
			}
		}

	        // Free allocated strings in evt (regardless of whether or not the event was empty).
	        free(evt.stick1);
	        free(evt.stick2);

		usleep(5000);
	}

}
